#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// Number of analyzer outputs per line
#define MAX_DISP_COLS 4

// LFSR LUT
#define LFSR_PERIOD 32767
uint16_t lfsr_state[LFSR_PERIOD];

uint16_t data_size;
uint16_t data_buf[32768];

#define MODE_WS 0
#define MODE_WSC 1
#define MODE_SC 2
#define MODE_WSC_MONO 3
#define MODE_SC_MONO 4

int running = 1;
int g_mode;

typedef struct {
  uint16_t raw;
  int cycle;

  int diff;
} CycleState;

struct CycleAnalyzer;
typedef bool (*CycleAnalyzerCb)(const struct CycleAnalyzer *anl, const CycleState *stt, int *o_cyc);
typedef void (*CyclePrinterCb)(const struct CycleAnalyzer *anl, const CycleState *stt, int cyc);
typedef struct CycleAnalyzer {
  const char *name;
  CycleAnalyzerCb func;
  CyclePrinterCb printer;
} CycleAnalyzer;

bool cycanl_insw(const CycleAnalyzer *anl, const CycleState *stt, int *o_cyc)
{
  (void)anl;
  if(stt->raw == 0)
    return false;

  // INSW is pretty much always 6 cycles
  *o_cyc = stt->diff - 6;

  return *o_cyc >= 0;
}

bool cycanl_out(const CycleAnalyzer *anl, const CycleState *stt, int *o_cyc)
{
  (void)anl;
  if(!cycanl_insw(anl, stt, o_cyc))
    return false;

  // OUT is pretty much always 7 cycles
  *o_cyc -= 7;

  return *o_cyc >= 0;
}

bool cycanl_last(const CycleAnalyzer *anl, const CycleState *stt, int *o_cyc)
{
  (void)anl;
  *o_cyc = stt->diff;

  return *o_cyc > 0;
}

bool cycanl_break(const CycleAnalyzer *anl, const CycleState *stt, int *o_cyc)
{
  (void)anl;
  (void)stt;
  (void)o_cyc;
  return stt->raw == 0;
}
void cycprint_break(const CycleAnalyzer *anl, const CycleState *stt, int cyc)
{
  (void)anl;
  (void)stt;
  (void)cyc;
  printf("----BREAK----");
}

void cycprint_base(const CycleAnalyzer *anl, const CycleState *stt, int cyc)
{
  (void)stt;
  printf("%s%+5d", anl->name, cyc);
}
static const CycleAnalyzer s_analyzers[] = {
  { NULL, NULL, cycprint_base, },
  { "break", cycanl_break, cycprint_break, },
  { "last", cycanl_last, NULL, },
  { "INSW", cycanl_insw, NULL, },
  { "OUT", cycanl_out, NULL, },
  { NULL, NULL, NULL, },
};

// Iterate over the analyzers and print the information if valid
void cycle_analyze(const CycleState *stt, int ppos, int start_col)
{
  int cyc, cols, skip_nl;
  const CycleAnalyzer *anl;
  cols = start_col;
  skip_nl = 1;
  for(anl = s_analyzers+1; anl->func != NULL; anl++) {
    if(!anl->func(anl, stt, &cyc))
      continue;
    if(cols == 0 && !skip_nl) {
      printf("\n");
      printf("%*s", ppos,"");
    }
    skip_nl = 0;
    printf(" , ");
    if(anl->printer == NULL) {
      s_analyzers[0].printer(anl, stt, cyc);
    }else{
      anl->printer(anl, stt, cyc);
    }
    cols++;
    if(cols == MAX_DISP_COLS) {
      cols = 0;
    }
  }
}

uint8_t read8(FILE *fp)
{
  uint8_t v = 0xFF;
  running = fread(&v, 1, 1, fp);
  return v;
}

void start_dataread(FILE *fp)
{
  uint16_t size;
  uint8_t c;

  // Verify the header
  c = read8(fp);
  if(c != 0xAD)
    return;

  c = read8(fp);
  if(c != 0xBE)
    return;

  c = read8(fp);
  if(c != 0xEF)
    return;

  // Read the buffer
  // This will overwrite the previous contents, but that's right
  // This lets us use an appending log file without issue
  fread(&size, 2, 1, fp);
  fread(data_buf, size, 1, fp);
  data_size = size / 2;
}

void parse_dataread(void)
{
  CycleState stt;
  int i;
  stt.cycle = 0;
  for(i = 0; i < data_size; i++) {
    // Get LFSR position
    stt.raw = data_buf[i];
    data_buf[i] = lfsr_state[stt.raw];

    // If the LFSR has wrapped, keep the diff accurate
    if(data_buf[i] < stt.cycle && stt.raw) {
      stt.cycle -= LFSR_PERIOD;
    }
    // Calculate deltas
    stt.diff = data_buf[i] - stt.cycle;
    stt.cycle = data_buf[i];

    // Print the basic data
    printf("%04X: %04X (%5d)", i, stt.raw, stt.cycle);
    // Apparently mingw can't be standards compliant with %n or printf's return...
    // So instead we calculate the end position ourselves...
    int ppos = 4+2+4+2+5+1;

    // Run the cycle analyzers
    cycle_analyze(&stt, ppos, 0);

    printf("\n");
  }
}

int main(int argc, const char *argv[])
{
  uint16_t lfsr = 0;
  int i;
  if(argc < 2) {
    fprintf(stderr, "Converts serial log to cycle timings and raw data.\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t%s input.log [ws|wsc|wsc_mono|sc|sc_mono]\n", argv[0]);
    fprintf(stderr, "\n");
    fprintf(stderr, "If machine isn't specified or is invalid, sc is default.\n");
    return EXIT_FAILURE;
  }

  g_mode = MODE_SC;
  if(argc > 2) {
    if(strcmp(argv[2], "ws") == 0) {
      g_mode = MODE_WS;
    }else if(strcmp(argv[2], "wsc") == 0) {
      g_mode = MODE_WSC;
    }else if(strcmp(argv[2], "wsc_mono") == 0) {
      g_mode = MODE_WSC_MONO;
    }else if(strcmp(argv[2], "sc") == 0) {
      g_mode = MODE_SC;
    }else if(strcmp(argv[2], "sc_mono") == 0) {
      g_mode = MODE_SC_MONO;
    }
  }

  // Generate LFSR lookup table
  for(i = 0; i < LFSR_PERIOD; i++) {
    lfsr_state[lfsr] = i;
    lfsr = (lfsr << 1) | ((1 ^ (lfsr >> 7) ^ (lfsr >> 14)) & 1);
    lfsr &= 0x7FFF;
  }

  // Read the data file
  FILE *fp;
  fp = fopen(argv[1], "rb");
  uint8_t c;
  running = 1;
  while(running) {
    c = read8(fp);
    if(!running)
      break;
    if(c == 0xDE) {
      start_dataread(fp);
    }
  }
  fclose(fp);

  // Spit out the cooked data
  parse_dataread();
  return EXIT_SUCCESS;
}
