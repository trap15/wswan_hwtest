# wswan_hwtest

Infrastructure and tools for reverse engineering the Bandai WonderSwan.

## What is this shit?

* A WonderWitch program and test harness to run in-depth tests on hardware.
* A tool to parse the logs generated by the test harness.

### What do I need to run a test?

* A WonderSwan/Color/Crystal
* A WonderWitch with serial cable
* WonderWitch SDK (with LSI C-86)
* TeraTerm (or other serial terminal with binary logging)

### How do I build and run the test?

* `make` in the project directory
* Copy `wstest.fx` to your WWitch
* Open serial terminal and start logging
* Run wstest on your WWitch
* Build logparse (`make tools`)
* Run logparse on the log
* Grok the output

### How do I write a new test?

* Copy `src/testskel.p86` to a new file
* Change `src/dmatest.obj` in `Makefile` to your new object
* Look at `src/dmatest.p86` for a real-world example
* Potentially write a new cycle analyzer in `tool/logparse.c`

## Who's responsible for this garbage?

It's me, I'm responsible.

### Links

* <http://daifukkat.su/> - My website
* <http://bitbucket.org/trap15/wswan_hwtest/> - This repository

### Greetz

* Ryphecha
* Charles MacDonald
* austere
* \#raidenii - Forever impossible

### Licensing

All contents within this repository (unless otherwise specified) are licensed under the following terms:

The MIT License (MIT)

Copyright (C)2015 Alex "trap15" Marshall <trap15@raidenii.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
