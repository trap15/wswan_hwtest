#include "top.h"

BYTE g_out_data[8192];

WORD asm_test(void);

void main(int argc, char *argv[])
{
  WORD size, off;
  comm_open();

  /* Obvious enough canaries */
  memset(g_out_data, 0x5A, sizeof(g_out_data));
  /* Wait enough that I feel confident in this being the start of a vblank */
  sys_wait(4);
  /* Run dat test */
  off = asm_test();
  size = off - FP_OFF(g_out_data);

  /* Header */
  comm_send_string("\xDE\xAD\xBE\xEF");
  comm_send_block(&size, 2);
  /* Data */
  comm_send_block(g_out_data, size);
  comm_close();

  bios_exit();
}
