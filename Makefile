.SUFFIXES: .fx .bin .obj .a86 .c

CRT0=crt0jpn1.obj

TEST=dmatest

OUTNAME=wstest
FXNAME=wstest
OBJECTS=src/main.obj \
	src/$(TEST).obj \
	src/harness.obj
RSRC=

CP=cp -p
RM=rm -f

CFLAGS=-Isrc/ -I./
DEFINES=
ASFLAGS=

CLEANS=$(OBJECTS) $(OUTNAME).bin $(OUTNAME).map files

default: all

all:	$(FXNAME).fx $(STAGEFX)

tools:	logparse

$(FXNAME).fx: $(OUTNAME).bin
	mkfent $*.cf

$(OUTNAME).bin: $(OBJECTS)
	echo "$(OBJECTS)" > files
	lcc86 -a$(CRT0) -k-M -o $*.bin @files

src/rsrcdata.p86: $(RSRC)

%.obj: %.a86
	r86 $(ASFLAGS) -o $@ $<

%.obj: %.c
	lcc86 $(CFLAGS) -SC $(DEFINES) -o tmp.z86 $< $(CFLAG_SFX)
	cat asm_pfx tmp.z86 > tmp.y86
	r86 $(ASFLAGS) -m $< -o $@ tmp.y86
	rm tmp.z86 tmp.y86

%.a86: %.c
	lcc86 -SC $(CFLAGS) $(DEFINES) -o $@ $<

%.a86: %.p86
	cpp -o $@ $<

logparse: tool/logparse.c
	$(CC) -Wall -Wextra -Werror $< -o $@

clean:
	$(RM) $(CLEANS)
	$(RM) *.fx
	$(RM) logparse

fullclean:
	$(RM) $(CLEANS) $(RSRC)
	$(RM) *.fx
	$(RM) logparse

distclean:
	$(RM) $(CLEANS) $(RSRC)
